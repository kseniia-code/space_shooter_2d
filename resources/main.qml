import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2
import QtMultimedia 5.0

Item{
    id:root

    Item {
        id: play
        focus: false //need only on third window
        Keys.onPressed: {

        if(event.key === Qt.Key_Left){ moveLeft()}
        else if(event.key === Qt.Key_Right){ moveRight()}
        else if(event.key === Qt.Key_Up){ moveUp()}
        else if(event.key === Qt.Key_Down){ moveDown()}

        if(event.key === Qt.Key_1){shootLeft()}
        else if(event.key === Qt.Key_4){shootRight()}
        else if(event.key === Qt.Key_2){shootUp()}
        else if(event.key === Qt.Key_3){shootDown()}

        }
    }

    property string current_player : "unknown"
    property point player_pos : Qt.point(0,0)

    signal moveUp()
    signal moveDown()
    signal moveLeft()
    signal moveRight()

    signal shootLeft()
    signal shootRight()
    signal shootUp()
    signal shootDown()

    signal connectButtonToServer(string text, string text)
    signal connectButtonToGame()
    signal disconnectButtonFromGame()

    states:[
        State{name:"state_lobby_window";
            PropertyChanges {
                target: lobby_window;
                visible:true
            }
            PropertyChanges {
                target: welcome_window;
                visible:false
            }

        },

        State{name:"state_play_window";
            PropertyChanges {
                target: play_window;
                visible:true
            }

            PropertyChanges {
                target: welcome_window;
                visible:false
            }
            PropertyChanges {
                target: play;
                focus:true
            }

        }

    ]


    Rectangle {

        id:welcome_window
        anchors.fill: parent
        color: "lightblue"
        Image{source:"qrc:/qml/space"}

        FocusScope {
            TextField {
                id: ip_server
                x: 330
                y: 250
                focus: true
                width: 160
                placeholderText: qsTr("Enter IP")
                text: ("127.0.0.1")
            }
        }

        FocusScope {
            TextField {
                id: port_server
                x: 330
                y: 280
                width: 160
                focus: true
                placeholderText: qsTr("Enter port")
                text: ("1234")
            }
        }



        Rectangle {
            property string text: "Connect to Server"
            radius: 7;
            x: 335
            y: 330

            width: 150
            height: 62
            color: mouse.pressed ?  "lightsteelblue" : "lightblue"
            Text
            {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                text: parent.text
                color: "black"
            }
            MouseArea
            {
                id: mouse
                anchors.fill: parent
                onClicked: {connectButtonToServer(ip_server.text, port_server.text); root.state="state_lobby_window"}
            }
        }

    }

    Rectangle {

        id:lobby_window
        anchors.fill: parent
        visible:false
        color: "lightsteelblue"


        ListModel {
            id: myModel
            ListElement {
                name: "Space Shooter"
            }
            ListElement {
                name: "Space Arcade"
            }
            ListElement {
                name: "Space Game"
            }
        }

        Component {
            id: game_choose
            Item {
                width: 180; height: 40
                Column {
                    Rectangle {
                        color:  mouse.onEntered ? "pink" : "lightblue"
                        width: 180; height: 40
                        border.width:1;
                        Text {
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            anchors.fill: parent
                            text: name }
                        MouseArea
                        {
                            id: mouse
                            anchors.fill: parent
                            onEntered: parent.color = "lightblue"
                        }

                    }
                }
            }
        }

        ListView {
            width: 180; height: 200
            x: 330
            y: 200
            model: myModel
            delegate: game_choose
            focus: true
        }


        Rectangle {
            property string text: "Connect to Game"
            x: 330
            y: 390
            radius: 7;
            width: 150
            height: 62
            color: mouse1.pressed ?  "lightsteelblue" : "lightblue"
            Text
            {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                text: parent.text
                color: "black"
                //font.pointSize: 10
            }
            MouseArea
            {
                id: mouse1
                anchors.fill: parent
                onClicked: {connectButtonToGame(); root.state="state_play_window"}
            }
        }


    }


    Rectangle {
        id:play_window
        anchors.fill: parent
        visible:false
        color: "lightsteelblue"

        Component {
            id: list_delegate

            RowLayout{
                height: 40; spacing: 30;
                Rectangle{ color: is_enemy ? "red" : "blue"; width: 40; Layout.fillHeight: true }
                Item{ Layout.fillWidth: true }
                Text{ text: name + " (" + score + ")" }
            }
        }

        Component {
            id: player_visu_delegate

            RowLayout {
                id: me
                x: position.x
                y: position.y
                Rectangle{ Image{source: is_enemy ? "qrc:/qml/ufo2" : "qrc:/qml/ufo1"}}
                Text{ height: 20; text: name }
            }
        }

        Component {
            id: bullet_visu_delegate

            RowLayout {
                id: bullet
                x: position.x
                y: position.y

                Rectangle{ width: 10; height: 10; radius: 5; color: "white" }


            }
        }

        RowLayout {
            anchors.fill: parent

            Rectangle {
                Image{source:"qrc:/qml/space"}

                Layout.fillWidth: true
                Layout.fillHeight: true

                clip: true

                Repeater {
                    anchors.fill: parent
                    model: VisualDataModel {
                        model: player_model
                        delegate: player_visu_delegate
                    }

                }

                Repeater {
                    anchors.fill: parent
                    model: VisualDataModel {
                        model: bullet_model
                        delegate: bullet_visu_delegate
                    }

                }

            }

            ListView{

                Layout.fillHeight: true
                width: 200

                model: VisualDataModel {
                    model: player_model
                    delegate: list_delegate
                }
            }
        }

        //button for disconnecting from game
        Rectangle {

            property string text: "Disconnect from Game"
            anchors.rightMargin: 30
            anchors.topMargin: 500
            anchors.top: parent.top
            anchors.right: parent.right
            width: 150
            height: 62
            radius: 7;
            color: mouse2.pressed ?  "lightsteelblue" : "lightblue"

            Text
            {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                text: parent.text
                color: "black"

            }

            MouseArea
            {
                id: mouse2
                anchors.fill: parent
                onClicked: disconnectButtonFromGame()
            }
        }

    }
}



