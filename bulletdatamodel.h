#ifndef BULLETDATAMODEL_H
#define BULLETDATAMODEL_H

// qt
#include <QPoint>
#include <QAbstractListModel>
#include <QDebug>
#include <QTimer>
#include <QTimerEvent>

//stl
#include<algorithm>

namespace Private {

  struct BulletData {
    explicit BulletData() : name{}, pos{} {}

    explicit BulletData(const QString& name_in, const QPoint& pos_in )
    : name{name_in}, pos{pos_in} {}

    QString   name;
    QPoint    pos;

  }; // END ShootData struct

}


class BulletDataModel : public QAbstractListModel {
  Q_OBJECT
public:
  enum DataRoles : int {
    NameRole = Qt::UserRole + 1,
    PositionRole

  };

  using QAbstractListModel::QAbstractListModel;

  void                            removeBullet( const QString& name );
  void                            timerEvent(QTimerEvent *event);

  QHash<int, QByteArray>          roleNames() const override;
  int                             rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant                        data(const QModelIndex &index, int role) const override;

public slots:
  void                            shootPlayer( const QString& name, const QPointF& dir, const QPoint& pos );
  void                            UpdateBullet(const QString& name, const QPoint& pos );

private:
  using BulletDataTuple = std::tuple<QVector<Private::BulletData>::Iterator,QModelIndex>;

  BulletDataTuple                 findBullet( const QString& name );

  QVector<Private::BulletData>    _data;
  QTimer                         _timer;
  QString                        _name;
  QPointF                        _direction;
  QPoint                         _pos;
  int i;


}; // END class BulletDataModel

#endif // BULLETDATAMODEL_H
