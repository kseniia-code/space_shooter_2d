#include "playerdatamodel.h"

/* This class is based on the playerdatamodel.cpp class in the
 *  example given by the Jostein Bratlie.
 */

void
PlayerDataModel::addPlayer(const QString& name, const QPoint& pos, int score, bool is_enemy ) {
    for(int i=0; i < _data.size(); i++) {
        if (_data[i].name==name){_a=1;} else {_a=0;}
    }
    if (_data.size()==0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _data.append(Private::PlayerData(name,pos,score,is_enemy));
        endInsertRows();
    }
    else if (_a==0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _data.append(Private::PlayerData(name,pos,score,is_enemy));
        endInsertRows();
    }
}


void
PlayerDataModel::removePlayer(const QString& name ) {
    for(int i=0; i < _data.size(); i++) {
        if (_data[i].name == name) {
            beginRemoveRows(QModelIndex(),i,i);
            _data.remove(i);
            emit(dataChanged(index(i), index(i)));
            endRemoveRows();
        }
    }
}

void
PlayerDataModel::UpdatePlayer(const QString& name, const QPoint& pos, int score, bool is_enemy ) {

    for(int i=0; i < _data.size(); i++) {
        if (_data[i].name == name) {
            _data.replace(i ,Private::PlayerData(name,pos,score,is_enemy));
            emit(dataChanged(index(i), index(i)));
        }
    }

}


PlayerDataModel::PlayerDataTuple
PlayerDataModel::findPlayer(const QString& name) {

    auto item = std::find_if( std::begin(_data), std::end(_data),
                              [name](const Private::PlayerData& data) { return data.name == name; }
    );

    return PlayerDataTuple(item,index(item-std::begin(_data)));
}


QHash<int,QByteArray>
PlayerDataModel::roleNames() const {

    QHash<int,QByteArray> roles;
    roles[DataRoles::NameRole] = "name";
    roles[DataRoles::PositionRole] = "position";
    roles[DataRoles::IsEnemyRole] = "is_enemy";
    roles[DataRoles::ScoreRole] = "score";
    return roles;
}

int
PlayerDataModel::rowCount(const QModelIndex&) const {

    return _data.size();
}

QVariant
PlayerDataModel::data(const QModelIndex& index, int role) const {

    if( !index.isValid() ) return QVariant();

    const auto& data = _data[index.row()];
    if( role == NameRole )      return QVariant(data.name);
    if( role == PositionRole )  return QVariant(data.pos);
    if( role == ScoreRole )     return QVariant(data.score);
    if( role == IsEnemyRole )   return QVariant(data.is_enemy);

    return QVariant();
}

void PlayerDataModel::movePlayer(const QString& name, const QPointF& dir) {

    auto itr = findPlayer(name);
    if(std::get<0>(itr) == std::end(_data))
        return;

    std::get<0>(itr)->pos += dir.toPoint( );

    //qDebug() << "Moving ";
    emit dataChanged(std::get<1>(itr),std::get<1>(itr));

}
