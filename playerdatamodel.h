#ifndef PLAYERDATAMODEL_H
#define PLAYERDATAMODEL_H


// qt
#include <QPoint>
#include <QAbstractListModel>
#include <QDebug>

//stl
#include<algorithm>


namespace Private {

  struct PlayerData {
    explicit PlayerData() : name{}, pos{}, is_enemy{}, score{} {}

    explicit PlayerData(const QString& name_in, const QPoint& pos_in,
                        int score_in, bool is_enemy_in )
    : name{name_in}, pos{pos_in}, is_enemy{is_enemy_in}, score{score_in} {}

    QString   name;
    QPoint    pos;
    bool      is_enemy;
    int       score;

  }; // END PlayerData struct

}


class PlayerDataModel : public QAbstractListModel {
  Q_OBJECT
public:
  enum DataRoles : int {
    NameRole = Qt::UserRole + 1,
    PositionRole,
    ScoreRole,
    IsEnemyRole
  };

  using QAbstractListModel::QAbstractListModel;

  void                            addPlayer( const QString& name, const QPoint& pos, int score, bool is_enemy = true );
  void                            removePlayer( const QString& name );
  void                            UpdatePlayer( const QString& name, const QPoint& pos, int score, bool is_enemy );

  QHash<int, QByteArray>          roleNames() const override;
  int                             rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant                        data(const QModelIndex &index, int role) const override;

  using PlayerDataTuple = std::tuple<QVector<Private::PlayerData>::Iterator,QModelIndex>;
  PlayerDataTuple                 findPlayer( const QString& name );
  QVector<Private::PlayerData>    _data;

public slots:
  void                            movePlayer( const QString& name, const QPointF& dir );

private:


  int _a;

}; // END class PlayerDataModel


#endif // PLAYERDATAMODEL_H
