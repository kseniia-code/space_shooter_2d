#include "gamerequest.h"
// stl
#include <cstring>

#include <QDebug>

/* This class is based on the protocoltestsui.cpp class in the
 * GameProtocol_client example given by the Jostein Bratlie.
 */

class Client;

GameRequest::GameRequest(QObject *parent) :
    QObject(parent)
{
    _mspeed = "10";

}

GameRequest::~GameRequest()
{

}

void GameRequest::moveRight() {

    sendGameRequest( "move", _mspeed, "0" );
    emit signRight();
}

void GameRequest::moveUp() {

    sendGameRequest( "move", "0", "-"+_mspeed );
    emit signUp();
}

void GameRequest::moveDown() {

    sendGameRequest( "move", "0", _mspeed );
    emit signDown();
}

void GameRequest::moveLeft() {

    sendGameRequest( "move", "-"+_mspeed, "0" );
    emit signLeft();
}

void GameRequest::shootRight() {

    sendGameRequest( "shoot", "1", "0" );

}

void GameRequest::shootDown() {

    sendGameRequest( "shoot", "0", "1" );

}

void GameRequest::shootUp() {

    sendGameRequest( "shoot", "0", "-1" );

}

void GameRequest::shootLeft() {

    sendGameRequest( "shoot", "-1", "0" );

}

bool GameRequest::sendGameRequest(const QString &cmd,
                                  const QString &p1,
                                  const QString &p2 ) {

    QStringList sl;
    sl << p1;
    sl << p2;

    return sendGameRequest( cmd, sl );
}

bool GameRequest::sendGameRequest(const QString &cmd, const QStringList &params) {

    gp_game_request request;

    // Check that the command is between 1 and 16 chars.
    if( cmd.size() < 1 || cmd.size() > 16 )
        return false;

    // Copy the command to the request struct
    std::strcpy( request.cmd, cmd.toStdString().c_str() );

    // Set the number of command parameters
    request.param_count = params.size();

    // Copy over the parameters to the request struct
    QStringList::ConstIterator itr;
    int i;
    for( i = 0, itr = params.begin(); itr != params.end(); itr++ ) {

        if( (*itr).size() < 1 || (*itr).size() > 16 )
            return false;

        std::strcpy( request.params[i++].param, (*itr).toStdString().c_str() );
    }

    qDebug() << "Entered command:";
    qDebug() << "  cmd: " << request.cmd;
    for( unsigned int i = 0; i < request.param_count; i++ )
        qDebug() << "  param[" << i << "]: " << request.params[i].param;

    emit signSendGameRequest( request );

    return true;
}




















