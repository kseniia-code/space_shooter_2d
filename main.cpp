#include <iostream>
#include "guiapplication.h"
#include "client.h"
#include "window.h"


int main (int argc, char * argv[])
{
    GuiApplication a(argc, argv);

    a.initialize();
    return a.exec();
}
