#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include "window.h"

//qt
#include <QGuiApplication>

class GuiApplication : public QGuiApplication {

    Q_OBJECT

public:
   explicit GuiApplication (int& argc, char ** argv);

    void initialize();

private:
    Window _window;

};

#endif // GUIAPPLICATION_H
