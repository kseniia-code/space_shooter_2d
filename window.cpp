#include "window.h"
#include "client.h"
#include "gameprotocol.h"

#include <QHostAddress>
#include <QDataStream>
#include <QQuickItem>
#include <QDebug>
#include <QtNetwork>
#include <QTcpSocket>
#include <QQmlContext>


class Client;
class GameRequest;

Window::Window(QWindow *parent) : QQuickView(parent){
    is_enemy=true;

    setGeometry( QRect(100,100, 800,600) );
    setMinimumSize (QSize(800, 600));
    setResizeMode(QQuickView::SizeRootObjectToView);

    setSource(QUrl("qrc:/qml/main_qml"));

    rootContext()->setContextProperty( "player_model", &_player_data_model);
    rootContext()->setContextProperty( "bullet_model", &_bullet_data_model);

    //1 Connect to connection server
    connect ( rootObject(), SIGNAL(connectButtonToServer(QString, QString)),this, SLOT(connectToHostServer( QString, QString )) );
    connect ( this, SIGNAL (signConnect(QString,int)), &_client, SLOT (connectToServer(QString,int)));

    //2 Send connect request
    connect ( &_client, SIGNAL(signClientConnectedToHost()),this, SLOT(windowConnectRequest()));
    connect(this, SIGNAL(signsendConnectRequest( gp_connect_request )),&_client,SLOT(sendConnectRequest( gp_connect_request )));

    //3 Recive connect request answer (receive client_id)
    connect(&_client, SIGNAL(signHandleConnectAnswerPkg( gp_header , gp_connect_answer )),this,SLOT(handleConnectAnswerPkg( gp_header , gp_connect_answer )));

    //4 DSQ request (receive number of game port)
    connect(this, SIGNAL(signsendDSQRequest( const gp_default_server_query )),&_client,SLOT(sendDSQRequest( const gp_default_server_query )));
    connect(&_client, SIGNAL(signHandleDSQAnswerPkg( gp_header , gp_default_server_query_answer )),
            this,SLOT(handleDSQAnswerPkg( gp_header , gp_default_server_query_answer )));

    //5 Send join request and get join request answer (receive validation code)
    connect(this, SIGNAL(signsendJoinRequest( const gp_join_request )),&_client,SLOT( sendJoinRequest( const gp_join_request )));
    connect(&_client, SIGNAL(signHandleJoinAnswerPkg( gp_header , gp_join_answer )),this,SLOT( windowhandleJoinAnswerPkg( gp_header , gp_join_answer )));

    //6 Connect to game server
    //server send verification request
    //client send verification answer (send validation code)
    connect( rootObject(),SIGNAL(connectButtonToGame()), this, SLOT(connectToGameServer()) );
    connect ( this, SIGNAL (signConnectToGame(QString,int)), &_client, SLOT (connectToGame(QString,int)));
    //create object
    //connect(&_client, SIGNAL(CreateObject()), rootObject(), SIGNAL(createObject()));

    //7 Disconnect from the game
    connect ( rootObject(), SIGNAL(disconnectButtonFromGame()),this, SLOT(closeWindow()) );

    //8 Get updates
    //connect ( &_client, SIGNAL(signClientConnectedToGame()),&_client, SLOT(windowUpdateRequest()));
    connect(&_client, SIGNAL(signHandleGameUpdatePkg( gp_header , gp_game_update )), this, SLOT( windowUpdateRequest(gp_header, gp_game_update)));

    //9 Send game requests
    connect(&_gamerequest, SIGNAL(signSendGameRequest(gp_game_request)),&_client,SLOT(sendGameRequest(gp_game_request)));

    connect(this, SIGNAL(signShowPlayers(gp_game_update )), this, SLOT(ShowPlayers(gp_game_update )));
    connect(this, SIGNAL(signUpdatePlayers(gp_game_object )), this, SLOT(UpdatePlayers(gp_game_object )));

    connect(rootObject(), SIGNAL(moveUp()), &_gamerequest, SLOT(moveUp()));
    connect(rootObject(), SIGNAL(moveDown()), &_gamerequest, SLOT(moveDown()));
    connect(rootObject(), SIGNAL(moveLeft()), &_gamerequest, SLOT(moveLeft()));
    connect(rootObject(), SIGNAL(moveRight()), &_gamerequest, SLOT(moveRight()));

    connect(rootObject(), SIGNAL(shootUp()), &_gamerequest, SLOT(shootUp()));
    connect(rootObject(), SIGNAL(shootDown()), &_gamerequest, SLOT(shootDown()));
    connect(rootObject(), SIGNAL(shootLeft()), &_gamerequest, SLOT(shootLeft()));
    connect(rootObject(), SIGNAL(shootRight()), &_gamerequest, SLOT(shootRight()));

}

void Window::closeWindow(){
    this->close();
}

//1 Connect to connection server
void Window::connectToHostServer( QString _ip_server, QString _port_server){
    //port to integer
    server=_ip_server;
    emit signConnect( _ip_server, _port_server.toInt());
}

//2 Send connect request
void Window::windowConnectRequest(){
    //build connect_request query
    _data.connect_flag=true;
    emit signsendConnectRequest( _data );

}

//3 Recive connect request answer
void Window::handleConnectAnswerPkg( gp_header header, gp_connect_answer answer ){
    _client_id = answer.client_id;

    //move current player
    _current_player = QString::number(_client_id);
    rootObject()->setProperty ("current_player",_current_player);

    //Build dsq query
    _query.request_flags.server_info = true;
    _query.server_fields.host_name = true;
    _query.server_fields.connect_port = true;

    _query.request_flags.map_info = true;
    _query.map_fields.width    = true;
    _query.map_fields.height   = true;

    emit signsendDSQRequest( _query );

}

//4 DSQ request
void Window::handleDSQAnswerPkg(gp_header header, gp_default_server_query_answer answer)
{
    if( answer.server_info.fields.connect_port )
        _game_port = answer.server_info.connect_port;
    else
        qDebug() << "Can't get gameport";
    // check if the answer contains this information
    if( answer.server_info.fields.player_count) {
        num_p=answer.server_info.player_count;
        qDebug() << "Players:"<< answer.server_info.player_count;
    }
    // Build join_request

    _join_request.flags.join_flag = GP_JOIN_FLAG_JOIN;
    _join_request.flags.client_type = GP_JOIN_CLIENT_TYPE_PLAYER;
    _join_request.client_id = _client_id;
    strcpy(_join_request.player_name, "Player_name");//must be entered in GUI

    emit signsendJoinRequest( _join_request );
}

//5 Send join request and get join request answer
void Window::windowhandleJoinAnswerPkg( gp_header , gp_join_answer  ){
    qDebug() << "Join";
}

//6 Connect to game
void Window::connectToGameServer(){
    emit signConnectToGame(server, _game_port);

}

void Window::windowUpdateRequest(gp_header , gp_game_update _update){

    emit signShowPlayers(_update);
}

void Window::ShowPlayers(gp_game_update _update){

    qDebug() << "Debugform - updating " << _update.count << " game objects";
    for( unsigned int i = 0; i < _update.count; i++ ) {

        qDebug() << "  obj" << _update.list[i].id << ": " << _update.list[i].type;

        emit signUpdatePlayers(_update.list[i]);
    }
}

void Window::UpdatePlayers(gp_game_object _player){

    if (_player.type == QString("spawn")){
        //check enemy or not
        if (_player.id ==_client_id){is_enemy=false;}
        _player_data_model.addPlayer( QString::number(_player.id),QPoint(_player.matrix.getM31(),_player.matrix.getM32()) ,  0, is_enemy );
        _player_data_model.UpdatePlayer(QString::number(_player.id),QPoint(_player.matrix.getM31(),_player.matrix.getM32()),  0, is_enemy);

        if (_player.id==_client_id){_player_pos=QPoint(_player.matrix.getM31(),_player.matrix.getM32());}
        //dsq with server_info.player_count
    }

    //update scene
    if (_player.type == QString("move")){
        if (_player.id ==_client_id){is_enemy=false;}
        _player_data_model.UpdatePlayer(QString::number(_player.id),QPoint(_player.matrix.getM31(),_player.matrix.getM32()),  0, is_enemy);

        //find current player
        if (_player.id==_client_id){_player_pos=QPoint(_player.matrix.getM31(),_player.matrix.getM32());}
    }

    if (_player.type == QString("shoot")){
        auto position = _player_data_model.findPlayer(QString::number(_player.id));
        _player_shoot=std::get<0>(position)->pos;

        _bullet_data_model.shootPlayer(QString::number(_player.id),QPoint(_player.matrix.getM31(),_player.matrix.getM32()), _player_shoot);
        _bullet_data_model.UpdateBullet(QString::number(_player.id),QPoint(_player.matrix.getM31(),_player.matrix.getM32()));
    }

    if (_player.type == QString("die")){
        _player_data_model.removePlayer(QString::number(_player.id));

    }

    is_enemy=true;

}
