#ifndef GAMEREQUEST_H
#define GAMEREQUEST_H

#include "gameprotocol.h"

#include <QObject>

class GameRequest : public QObject
{
    Q_OBJECT

public:
    explicit GameRequest (QObject *parent = 0);
    ~GameRequest ();

  gp_connect_request        getConnectQueryPkg() const;
  gp_default_server_query   getDSQueryPkg() const;
  gp_join_request           getJoinRequestPkg() const;

signals:
    void                    signSendGameRequest( const gp_game_request& request );
    void signDown();
    void signUp();
    void signLeft();
    void signRight();

private:
    QString                 _mspeed;

    bool                    sendGameRequest( const QString& cmd,
                                             const QStringList& params );

    bool                    sendGameRequest( const QString &cmd,
                                             const QString &p1,
                                             const QString &p2 );

private slots:
    void                    moveUp();
    void                    moveDown();
    void                    moveLeft();
    void                    moveRight();

    void                    shootUp();
    void                    shootDown();
    void                    shootLeft();
    void                    shootRight();

};

#endif // GAMEREQUEST_H
