#ifndef WINDOW_H
#define WINDOW_H

class Client;
class GameRequest;

#include <QtGui>
#include <QQuickItem>
#include <QQuickView>
#include <QTcpSocket>
#include "client.h"
#include "gameprotocol.h"
#include "gamerequest.h"
#include "playerdatamodel.h"
#include "bulletdatamodel.h"
#include <QPoint>

namespace Ui {
class Window;
}

class Window : public QQuickView {
    Q_OBJECT

public:
    using QQuickView::QQuickView;
    explicit Window(QWindow* parent=0);

signals:

    void signConnect(QString ip, int port);
    void signConnectToGame(QString ip, int port);
    void signSendGameRequest(const gp_game_request& _request);
    void signsendConnectRequest( const gp_connect_request& query );
    void signsendDSQRequest( const gp_default_server_query query );
    void signHandleConnectAnswerPkg (gp_header, gp_connect_answer);
    void signHandleDSQAnswerPkg( gp_header, gp_default_server_query_answer );
    void signsendJoinRequest( const gp_join_request );
    void signHandleGameUpdate(gp_header header, gp_game_update update );
    void signUpdate(gp_header header, gp_game_update update);
    void windowHandleGameUpdatePkg(gp_header, gp_game_update);
    void signShowPlayers(gp_game_update _update);
    void signUpdatePlayers(gp_game_object _player);

private:
    bool                 is_enemy;
    int                  a,i,num_p;
    QPoint              _players[];
    QQuickItem          _root_item;
    GameRequest         _gamerequest;
    QString             _ip_server, _port_server, server, _current_player;
    quint16             _game_port;
    Client              _client;
    gp_connect_request  _data;
    gp_default_server_query _query;
    gp_join_request     _join_request;
    gp_game_request     _game_request;
    quint32             _client_id;
    gp_header           _header;
    gp_game_update      _update;
    PlayerDataModel     _player_data_model;
    gp_game_object      _player;
    BulletDataModel     _bullet_data_model;
    QPoint             _player_pos, _player_shoot;

private slots:
    void closeWindow();
    void connectToHostServer( QString, QString);
    void windowConnectRequest();
    void windowhandleJoinAnswerPkg( gp_header , gp_join_answer );
    void connectToGameServer();
    void handleConnectAnswerPkg( gp_header, gp_connect_answer);
    void handleDSQAnswerPkg( gp_header , gp_default_server_query_answer );
    void windowUpdateRequest(gp_header, gp_game_update);
    void ShowPlayers(gp_game_update _update);
    void UpdatePlayers(gp_game_object _player);

};


#endif // WINDOW_H
