#include "bulletdatamodel.h"
#include "playerdatamodel.h"

BulletDataModel::BulletDataTuple
BulletDataModel::findBullet(const QString& name) {

    auto item = std::find_if( std::begin(_data), std::end(_data),
                              [name](const Private::BulletData& data) { return data.name == name; }
    );

    return BulletDataTuple(item,index(item-std::begin(_data)));
}

void
BulletDataModel::removeBullet(const QString& name ) {
    for(int i=0; i < _data.size(); i++) {
        if (_data[i].name == name) {
            beginRemoveRows(QModelIndex(),i,i);
            _data.remove(i);
            emit(dataChanged(index(i), index(i)));
            endRemoveRows();
        }
    }
}


QHash<int,QByteArray>
BulletDataModel::roleNames() const {

    QHash<int,QByteArray> roles;
    roles[DataRoles::NameRole] = "name";
    roles[DataRoles::PositionRole] = "position";
    return roles;
}

int
BulletDataModel::rowCount(const QModelIndex&) const {

    return _data.size();
}

QVariant
BulletDataModel::data(const QModelIndex& index, int role) const {

    if( !index.isValid() ) return QVariant();

    const auto& data = _data[index.row()];
    if( role == NameRole )      return QVariant(data.name);
    if( role == PositionRole )  return QVariant(data.pos);

    return QVariant();
}

void
BulletDataModel::UpdateBullet(const QString& name, const QPoint& pos ) {

    for(int i=0; i < _data.size(); i++) {
        if (_data[i].name == name) {
            _data.replace(i ,Private::BulletData(name,_pos));
            emit(dataChanged(index(i), index(i)));
        }
    }

}

void BulletDataModel::shootPlayer(const QString& name, const QPointF& dir, const QPoint& pos) {

    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    _data.append(Private::BulletData(name,pos));
    endInsertRows();
    i=0;
    _direction=dir;
    _name=name;
    _pos=pos;

    auto itr = findBullet(name);
    std::get<0>(itr)->pos=_pos;
    if(std::get<0>(itr) == std::end(_data))
        return;
    startTimer(200);

}

void BulletDataModel::timerEvent(QTimerEvent *event) {

    auto itr = findBullet(_name);
    i++;
    std::get<0>(itr)->pos += (_direction.toPoint());

    emit dataChanged(std::get<1>(itr),std::get<1>(itr));
    if (i==50){
        killTimer(event->timerId());
        removeBullet(_name);
        i=0;
        }
}

